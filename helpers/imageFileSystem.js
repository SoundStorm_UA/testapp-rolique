'use strict';

module.exports = function (options) {
    var im = require('imagemagick');
    var async = require('async');

    var rootDir = options.rootDir || 'public';

    var defaultImageDir = options.defaultImageDir || 'images';

    var fs = require('fs');
    var os = require('os');

    var osPathData = getDirAndSlash();

    function getDirAndSlash() {
        var osType = os.type().split('_')[0];
        var slash;
        var dir;
        var webDir = process.env.HOST;
        var ROOT_DIR = process.env.ROOT_FOLDER;

        if (osType === 'Linux') {
            slash = '\/';
        } else if ('Windows') {
            slash = '\\';
        }

        dir = ROOT_DIR.concat(slash, rootDir, slash);

        return {dir: dir, slash: slash, webDir: webDir};
    }

    function encodeFromBase64(dataString, callback) {
        var inValidError = new Error();
        var matches;
        var imageData = {
            type     : 'image/png',
            extension: 'png'
        };
        var imageTypeDetected;

        inValidError.code = 400;
        inValidError.message = 'Invalid input string';

        if (!dataString) {
            return callback(inValidError);
        }

        matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

        if (matches && matches.length === 3) {
            imageData.type = matches[1];
            imageTypeDetected = imageData.type.match(/\/(.*?)$/);
            imageData.extension = (imageTypeDetected[1] === "svg+xml") ? "svg" : imageTypeDetected[1];
            dataString = matches[2];
        }

        try {
            imageData.data = new Buffer(dataString, 'base64');
        } catch (err) {
            return callback(inValidError);
        }

        callback(null, imageData);
    }

    function writer(path, options, callback) {
        var imagePath = path + options.originalFilename;

        fs.writeFile(imagePath, options.data, function (err) {
            if (err) {
                return callback(err);
            }

            options.url = imagePath;

            return callback(null, options);
        });
    }

    function dirCheck(dir, cb) {
        fs.readdir(dir, function (err) {
            if (err) {
                fs.mkdir(dir, function (err) {
                    if (err) {
                        return cb(err);
                    }

                    return cb(null);
                });
            } else {
                return cb(null);
            }
        });
    }

    function saveImage(options, callback) {
        var dir = options.dirToSave;
        var folderName = options.originalSubDir;

        var slash = osPathData.slash;
        var fullDir = dir + folderName + slash;

        async.waterfall([
            async.apply(dirCheck, osPathData.dir),
            async.apply(dirCheck, dir),
            async.apply(dirCheck, fullDir),
            async.apply(writer, fullDir, options)
        ], function (err, _opts) {
            var checkCbExists = callback && typeof callback === 'function';

            if (err) {
                return checkCbExists && callback(err);
            }

            return checkCbExists && callback(null, _opts);
        });
    }

    this.getImagePath = function (imageName, folderName) {
        var slash = osPathData.slash;
        var url = process.env.HOST.concat(defaultImageDir, slash, (folderName && (folderName + slash)), imageName);

        return url;
    };

    this.readFile = function (options, callback) {
        fs.readFile(options.tempPath, function (err, buffer) {
            var ifCallBack = callback && typeof callback === 'function';

            if (err) {
                if (ifCallBack) {
                    return callback(err);
                }

                return err;
            }

            options.data = buffer;

            if (ifCallBack) {
                return callback(null, options);
            }

            return options;
        });
    };

    this.uploadImage = function (options, callback) {
        var slash = osPathData.slash;

        options.dirToSave = osPathData.dir + defaultImageDir + slash;

        saveImage(options, callback);
    };

    this.cropImage = function (options, callback) {
        var slash = osPathData.slash;

        var dir = osPathData.dir + defaultImageDir + slash;
        var folderName = options.partsSubDir;
        var fullDir = dir + folderName + slash;

        var args = [
            options.tempPath,
            '-crop',
            (100 / options.partsCount) + '%x100%',
            '+repage',
            fullDir + options.nameWithoutExtension + '_%d' + options.extension
        ];

        function cropper(options, args, callback) {
            im.convert(args, function (err) {
                if (err) {
                    return callback(err);
                }

                return callback(null, options);
            });
        }

        async.waterfall([
            async.apply(dirCheck, osPathData.dir),
            async.apply(dirCheck, dir),
            async.apply(dirCheck, fullDir),
            async.apply(cropper, options, args)
        ], function (err, _opts) {
            var checkCbExists = callback && typeof callback === 'function';
            var partName;

            if (err) {
                return checkCbExists && callback(err);
            }

            options.partsUrls = {};

            for (var i = 0; i < options.partsCount; i++) {
                partName = options.nameWithoutExtension + '_' + i + options.extension;
                options.partsUrls[partName] = fullDir + partName
            }

            return checkCbExists && callback(null, _opts);
        });

    };

    return this;
};
