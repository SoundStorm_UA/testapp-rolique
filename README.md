# testApp

## Tech

### Installation

Please make sure to have installed:
- [Git](https://git-scm.com/);
- [Node.js](https://nodejs.org/) 4+;

Clone the project.

```sh
$ git clone https://bitbucket.org/SoundStorm_UA/testapp-rolique.git
```

Install:

```sh
$ cd testapp-rolique
$ npm install
```

### Run

You will be able to run this project:

```sh
$ node server.js
```

After run server will start on PORT 3333.

### Test

To test routes you can use postman:

 * create: 
    - url: [localhost:3333/file/](http://localhost:3333/file/);
    - headers: "Content-Type": "application/json";
    - body: form-data & add img file;
    
 * getAll: 
    - url: [localhost:3333/file/](http://localhost:3333/file/) + query if you need it. 'original' key 
    can be passed to query with boolean value [try](localhost:3333/file/?original=true);
    - headers: "Content-Type": "application/json";
    
    * getById: 
    - url: http://localhost:3333/file/<id>;
    - headers: "Content-Type": "application/json";
