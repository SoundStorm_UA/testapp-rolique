'use strict';

module.exports = function (db) {
    var _ = require('lodash');
    var async = require('async');
    var mongoose = require('mongoose');
    var ImageUploader = require('../helpers/imageFileSystem');
    var imageUploader = new ImageUploader({
        rootDir        : 'public',
        defaultImageDir: 'images'
    });
    var Model = db.models.file;
    var ObjectId = mongoose.Types.ObjectId;

    var defProjection = {
        name       : 1,
        createdDate: 1,
        editedDate : 1
    };

    this.getAll = function (req, res, next) {
        var query = req.query;
        var count = parseInt((query.count || 10), 10);
        var page = parseInt((query.page || 0), 10);
        var skip = page * count;
        var $matchObject = {};
        var original;

        if (query.hasOwnProperty('original')) {
            original = query.original === 'false';
            $matchObject.parts = {$exists: !original};
        }

        function getData(cb) {
            var pipeLine = [];

            pipeLine.push({
                $match: $matchObject
            });

            pipeLine.push({
                $project: defProjection
            });

            pipeLine.push({
                $skip: skip
            });

            pipeLine.push({
                $limit: count
            });

            Model.aggregate(pipeLine, function (err, result) {
                if (err) {
                    return cb(err);
                }

                cb(null, result);
            })
        }

        function getTotal(cb) {
            Model.count($matchObject, function (err, count) {
                if (err) {
                    return cb(err);
                }

                cb(null, count);
            })
        }

        async.parallel({
            data : getData,
            total: getTotal
        }, function (err, results) {
            if (err) {
                return next(err);
            }

            res.status(200).send(results);
        })
    };

    this.getById = function (req, res, next) {
        var params = req.params;
        var id = params.id;

        var pipeLine = [];

        pipeLine.push({
            $match: {'_id': ObjectId(id)}
        });

        pipeLine.push({
            $unwind: {
                path                      : '$parts',
                includeArrayIndex         : 'partIndex',
                preserveNullAndEmptyArrays: true
            }
        });

        pipeLine.push({
            $lookup: {
                from        : 'files',
                localField  : 'parts',
                foreignField: '_id',
                as          : 'parts'
            }
        });

        pipeLine.push({
            $sort: {'_id': 1, 'partIndex': 1}
        });

        pipeLine.push({
            $group: {
                _id        : '$_id',
                name       : {$first: '$name'},
                url        : {$first: '$url'},
                type       : {$first: '$type'},
                size       : {$first: '$size'},
                createdDate: {$first: '$createdDate'},
                editedDate : {$first: '$editedDate'},
                parts      : {$addToSet: '$parts'}
            }
        })

        Model.aggregate(pipeLine, function (err, result) {
            if (err) {
                return next(err);
            }

            return res.status(200).send(result);
        });
    };

    this.createFile = function (req, res, next) {
        var error;
        var body = req.body;
        var files = req.files;
        var lastDotIndex;
        var fileOptions;
        var originalFileName;
        var arrayOfFiles = [];
        var file;

        function saveToDB(options, callback) {
            var parallelTasks = [];
            var parts = options.partsUrls;
            var partOptions;

            function saveData(data, cb) {
                var model = new Model(data);

                model.save(function (err, document) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null, document);
                });
            }

            for (var key in parts) {
                if (parts.hasOwnProperty(key)) {
                    partOptions = {
                        name: key,
                        url : parts[key],
                        type: options.type
                    };

                    parallelTasks.push(async.apply(saveData, partOptions));
                }
            }

            async.waterfall([
                function (wtfCb) {
                    async.parallel(parallelTasks, function (err, results) {
                        if (err) {
                            return wtfCb(err);
                        }

                        options.parts = _.map(results, '_id');

                        return wtfCb(null, options);
                    })
                },
                saveData
            ], function (err, result) {
                if (err) {
                    return callback(err);
                }

                return callback(null, result);
            });
        }

        if (!files) {
            error = new Error();
            error.status = 400;
            error.message = 'Not enough parameters.';

            return next(error);
        }

        for (var key in files) {
            arrayOfFiles = arrayOfFiles.concat(files[key]);
        }

        file = arrayOfFiles[0];

        originalFileName = file.originalFilename;

        fileOptions = {
            type            : file.type,
            originalFilename: originalFileName,
            tempPath        : file.path,
            originalSubDir  : 'original',
            partsSubDir     : 'parts',
            size            : file.size,
            partsCount      : body.count ? parseInt(body.count, 10) : 3
        };

        lastDotIndex = originalFileName.lastIndexOf('.');

        fileOptions.extension = originalFileName.substr(lastDotIndex);
        fileOptions.nameWithoutExtension = originalFileName.substr(0, lastDotIndex);

        async.waterfall([
            async.apply(imageUploader.cropImage, fileOptions),
            imageUploader.readFile,
            imageUploader.uploadImage,
            saveToDB
        ], function (err, result) {
            if (err) {
                return next(err);
            }

            res.status(200).send(result)
        });
    };
};