module.exports = function () {
    var mongoose = require('mongoose');
    var ObjectId = mongoose.Schema.Types.ObjectId;

    var fileSchema = mongoose.Schema({
        name : {type: String},
        type : {type: String},
        url  : {type: String},
        size : {type: Number},
        parts: {type: [{type: ObjectId, ref: 'file'}], default: void 0},

        createdDate: {type: Date, default: Date.now},
        editedDate : {type: Date, default: Date.now}
    }, {collection: 'files'});

    mongoose.model('file', fileSchema);
};
