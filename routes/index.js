'use strict';

module.exports = function (app, db) {
    var fileRouter = require('./file')(db);

    app.use('/file', fileRouter);
};