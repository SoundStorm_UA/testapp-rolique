'use strict';

var express = require('express');
var router = express.Router();

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var Handler = require('../handlers/file');

module.exports = function (db) {
    var handler = new Handler(db);

    router.get('/', handler.getAll);

    router.post('/', multipartMiddleware, handler.createFile);

    router.get('/:id', handler.getById);

    /* router.delete('/', handler.remove); */

    return router;
};
