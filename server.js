'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var http = require('http');
var httpServer;
var db;
var app;
var config = {
    DB_HOST    : 'localhost',
    DB_NAME    : 'testROLIQUE',
    HOST       : 'localhost',
    PORT       : 3333,
    ROOT_FOLDER: __dirname
};

var url = 'mongodb://' + config.DB_HOST + '/' + config.DB_NAME;

process.env = _.extend({}, process.env, config);

mongoose.Promise = global.Promise;
mongoose.connect(url);

db = mongoose.connection;

db.on('error', function (err) {
    console.dir('connection error: ' + err);
    // if pm2 then after error will restart and try again
    throw err;
});
db.once('open', function callback() {
    var port = config.PORT || 443;

    console.log('Connected to db is success');

    require('./models/index.js')();

    app = require('./app')(db);

    httpServer = http.createServer(app);

    httpServer.listen(port, function () {
        console.log('==========================================');
        console.log('|| Success! Server started on port=' + port + ' ||');
        console.log('==========================================\n');
    });
});