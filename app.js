module.exports = function (db) {
    var path = require('path');
    var express = require('express');
    var bodyParser = require('body-parser');
    var mongoose = require('mongoose');

    var app = express();

    app.use(bodyParser.json({strict: false, limit: 1024 * 1024 * 200}));
    app.use(bodyParser.urlencoded({extended: false, limit: 1024 * 1024 * 200}));

    app.use(express.static(path.join(__dirname, 'public')))

    require('./routes/index')(app, db);

    return app;
};